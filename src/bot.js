const TelegramBot = require('node-telegram-bot-api');

const token = '';

const bot = new TelegramBot(token, { polling: true });

bot.onText(/\/cmd(.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const resp = match[1];
    console.log(resp, msg)
    bot.sendMessage(chatId, resp);
});

bot.on('message', (msg) => {
    const chatId = msg.chat.id;
    console.log('message is', msg)
    bot.sendMessage(chatId, 'Received your message');
});